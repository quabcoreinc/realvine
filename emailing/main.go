package emailing

import (
	"net/smtp"
	"bytes"
	"html/template"
	"log"
	"strconv"
)

type EmailUser struct {
	Username    string
	Password    string
	EmailServer string
	Port        int
}

type SmtpTemplateData struct {
	From    string
	To      string
	Phone   string
	Subject string
	Body    string
}

func SendEmail(recipients []string, name string, phone string, email string, subject string, message string) error{
        log.Println("A mail sending begins")

	//configure smtp account for sending mail
	emailUser := &EmailUser{"info.realvinegh@gmail.com", "Vinegh@2017", "smtp.gmail.com", 587}


	auth :=smtp.PlainAuth("", emailUser.Username, emailUser.Password, emailUser.EmailServer)
	to:=""
	for _, v := range recipients {
		to = to + ", " + v
	}
	var emailTemplate =`From: &#123;&#123;.From&#125;&#125;
To: `+ to + `;
Subject: `+ subject+`;

Details: ` + message + `

Phone Number: ` + phone + `
Email: ` + email + `

Sincerely,
`+ name


	var err error
	var doc bytes.Buffer

	context := &SmtpTemplateData{
		name,
		to,
		phone,
		subject,
		"Details: " + message,
	}

	t :=template.New("emailTemplate")
	t, err =t.Parse(emailTemplate)
	if err != nil{
		log.Println("Error trying to parse mail template")
	}
	err = t.Execute(&doc, context)
	if err != nil{
		log.Println("Error trying to execute mail template")
	}

	err = smtp.SendMail(emailUser.EmailServer + ":"+strconv.Itoa(emailUser.Port),
				    auth, emailUser.Username,
	                            recipients,
		                     doc.Bytes())
	if err != nil{
		log.Println("An error occured while attempting to send a mail ", err)
	}else{
		log.Println("Email sent successfully!")
	}
      return err

}