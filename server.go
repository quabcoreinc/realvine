package main

import(
	"os"
	"fmt"
	"github.com/RealVineWeb/handlers"
)


func main(){
	 a := handlers.App{}
	 a.Initialize()
	 a.Run(getPort())	
}

func getPort() string{
	var port = os.Getenv("PORT")
	if port == ""{
		port ="8080"
		fmt.Printf("INFO: No PORT environment variable detected, defaulting to " + port)
	}
	return ":" + port
}


