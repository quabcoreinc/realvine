package main

import(
	"testing"
	"net/http"
	"net/http/httptest"
	"os"
	"github.com/RealVineWeb/handlers"	
	"net/url"
	"strings"
)

var a handlers.App


func executeRequest(req *http.Request) *httptest.ResponseRecorder{
	rr:= httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected,  actual int){
	 if expected != actual {
		 t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	 }
}

func TestMain(m * testing.M){
	a = handlers.App{}

	a.Initialize()
	
	code := m.Run()
	os.Exit(code)
}

func TestHomePage(t *testing.T){
	req, _ := http.NewRequest("GET", "/", nil)

	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestServicesPage(t *testing.T){
	req, _ := http.NewRequest("GET", "/services", nil)

	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestAdminPage(t *testing.T){
	req, _ := http.NewRequest("GET", "/admin", nil)

	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestBlogPage(t *testing.T){
	req, _ := http.NewRequest("GET", "/blog", nil)

	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestMakeOrder(t *testing.T){
	form := url.Values{}
	form.Add("flavor", "strawberry")
	form.Add("bottles", "20")
	form.Add("payment", "Mobile Money")

	req, _ := http.NewRequest("POST", "/contact", strings.NewReader(form.Encode()))

	response := executeRequest(req)

	checkResponseCode(t, http.StatusBadRequest, response.Code)
}

func TestGetSessionString(t *testing.T){
	req, _ := http.NewRequest("GET", "/admin", nil)

	sessionVal := a.GetStringFromSession(req, "yahoo")

	if sessionVal != "" {
		t.Errorf("Expected value was %s. Got %s\n", "", sessionVal)
	}
}