package helpers

import (
	"database/sql"
	"gopkg.in/gorp.v1"
	"github.com/RealVineWeb/models"
	"os"

	_"github.com/lib/pq"
	_"github.com/mattn/go-sqlite3"
)


type I interface {
	InitializeDb()
	PingDb() bool
	CloseDb() error
	GetUserByUsername(username string)(*models.User, error)
	AddPost(post *models.Post) error
	GetPostById(post *models.Post, pk int64) error
	GetPosts(post *[]models.Post) error
	DeletePost(post *models.Post) error
	GetEmails(email *[]models.Email) error
	UpdateEmail(email *models.Email) error
	AddEmail(email *models.Email) error
}

type T struct{
	db *sql.DB
}

var dbmap *gorp.DbMap

func (t *T) InitializeDb() {
	if os.Getenv("ENV") != "production" {
		       t.db, _ = sql.Open("sqlite3", "realv.db")
		       dbmap = &gorp.DbMap{Db: t.db, Dialect: gorp.SqliteDialect{},}


	}else {
		t.db, _=sql.Open("postgres", os.Getenv("DATABASE_URL"))
		dbmap = &gorp.DbMap{Db:t.db, Dialect:gorp.PostgresDialect{},}
	}

	dbmap.AddTableWithName(models.Post{},"posts").SetKeys(true, "pk")
	dbmap.AddTableWithName(models.User{},"users").SetKeys(false, "username")
	dbmap.AddTableWithName(models.Email{},"emails").SetKeys(true, "pk")
	dbmap.CreateTablesIfNotExists()

}

func (t *T) PingDb() bool{
	if err := t.db.Ping(); err!=nil{
		return false
	}

	return true;
}

func (t *T) CloseDb() error{
	return t.db.Close()
}

func (t *T)GetUserByUsername(username string)(*models.User, error) {
	user, err := dbmap.Get(models.User{}, username)
	if user != nil{
		return  user.(*models.User), err
	}else{
		return  nil, err
	}
}

func (t *T)AddPost(post *models.Post) error{
	return dbmap.Insert(post)
}

func (t *T)AddEmail(email *models.Email) error{
	return dbmap.Insert(email)
}

func(t *T)UpdateEmail(email *models.Email) error{
	if _,err := dbmap.Update(email); err!=nil{
		return err
	}
	return  nil
}

func (t *T)GetEmails(email *[]models.Email) error{
	query:="select * from emails where issent = False"
	if os.Getenv("ENV") != "production" {
		query = "select * from emails where issent = 0"
	}

	if _,err := dbmap.Select(email, query);err != nil{
		return err
	}
	return nil
}

func (t *T)GetPostById(post *models.Post, pk int64) error{
	if err := dbmap.SelectOne(post, "select * from posts where pk="+ dbmap.Dialect.BindVar(0), pk); err!=nil{
		return err
	}
	return nil
}

func (t *T)GetPosts(post *[]models.Post) error{
	if _,err := dbmap.Select(post, "select * from posts");err != nil{
		return err
	}
	return nil
}

func (t *T)DeletePost(post *models.Post) error{
	if _,err := dbmap.Delete(post); err!=nil{
		return err
	}
	return  nil
}


var activateEmailService = false
func ActivateEmailService()bool{
	return activateEmailService
}

type Page struct{
	Posts []models.Post
	Emails []models.Email
	User string
	Token string
	Error string
	Success string
}