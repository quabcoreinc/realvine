
<!DOCTYPE HTML>
<html>
<head>
    <title>Real Vine Juice | 404</title>
    <link href="../static/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Merriweather+Sans' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="header-box"></div>
<div class="wrap">
    <div class="total">
        <div class="header">
            <div class="header-bot">
                <div class="logo">
                    <a href="index.html"><img src="../static/images/logo.png" alt=""/></a>
                </div>
                <ul class="follow_icon">
                    <li><a href="#"><img src="../static/images/fb1.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/rss.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/tw.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/g+.png" alt=""></a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="search-bar">
                <input type="text" class="textbox" value=" Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
                <input name="searchsubmit" type="image" src="../static/images/search-icon.png" value="Go" id="searchsubmit" class="btn">
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="menu">
            <div class="top-nav">
                <ul>
                    <li class="active"><a href="/">Home</a></li> |
                    <li><a href="about">About</a></li> |
                    <li><a href="blog">Blog</a></li> |
                    <li><a href="services">Services</a></li> |
                    <li><a href="contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="page-not-found">
            <h1>404</h1>
        </div>
        <div class="clear"> </div>
    </div>
</div>
<div class="footer-bottom">
    <div class="wrap">
        <div class="copy">
            <p> © 2017 All rights Reserved | Powered by <a href="#">Innovation Africa</a></p>
        </div>
    </div>
</div>
</body>
</html>



