
<!DOCTYPE HTML>
<html>
<head>
    <title>Real Vine Juice  | Services </title>
    <link href="../static/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Merriweather+Sans' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="header-box"></div>
<div class="wrap">
    <div class="total">
        <div class="header">
            <div class="header-bot">
                <div class="logo">
                    <a href="index.html"><img src="../static/images/logo.png" alt=""/></a>
                </div>
                <ul class="follow_icon">
                    <li><a href="#"><img src="../static/images/fb1.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/rss.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/tw.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/g+.png" alt=""></a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="search-bar">
                <input type="text" class="textbox" value=" Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
                <input name="searchsubmit" type="image" src="../static/images/search-icon.png" value="Go" id="searchsubmit" class="btn">
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="menu">
            <div class="top-nav">
                <ul>
                    <li><a href="/">Home</a></li> |
                    <li><a href="about">About</a></li> |
                    <li><a href="blog">Blog</a></li> |
                    <li class="active"><a href="services">Services</a></li> |
                    <li><a href="contact">Contact</a></li>
                </ul>
            </div>
        </div>
        <div class="banner">
        </div>
    </div>
    <div class="main">
        <div class="section group">
            <div class="cont2 span_2_of_services services_desc">
                <h2>Our Services</h2>
                <p>TVP is planning to expand its operations this year, and this includes basically increasing distribution to as many shops as possible, partnering with many brands and making our products easily available to our cherished clients and the general public.</p>
                <p>We have four bottle sizes tha we use; 200ml, 330ml, 500ml and 750ml. we also give juice out in 5l gallons as per the requirement of the client..</p>
                <div class="image group">
                    <div class="grid images_3_of_1">
                        <img src="../static/images/b-img.jpg" alt=""/>
                    </div>
                    <div class="grid span_2_of_1">
                        <h3> </h3>
                        <p>We offer juice dispenser for refreshment,this would be on the clients premises. During occasions, such as coorperate and private parties, weddings, engagements, anniversary celebrationsand so on according to the requirement of the client. We can alsocustomize our lables with either text or pictures or both for special events.</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="image group">
                    <div class="grid images_3_of_1">
                        <img src="../static/images/b-img1.jpg" alt="">
                    </div>
                    <div class="grid span_2_of_1">
                        <h3> </h3>
                        <p>Our juice once bottled expires after 20 days, if for any reason the juice has not been ought or remains we will replace them after inspection. In case there is any anomaly with the product once verified y us, we have the obligation to replace the said quantity for you</p>
                    </div>
                    <div class="clear"></div>
                </div>

            </div>
            <div class="lsidebar2 sidebar2 offers_list">
                <h2>Distributors</h2>
                <ul>
                    <li>Est western premier hotel</li>
                    <li>Bake shop classics </li>
                    <li>Le Bijou</li>
                    <li>Marwako restaurant</li>
                    <li>Dotkad supermarket</li>
                    <li>Kinko ashgrove</li>
                    <li>Green grass grill</li>
                    <li>Adepa aduane</li>
                    <li>Fuel station marts across the capital</li>
                </ul>
                <div class="archives">
                    <h2>Achivements</h2>
                    <ul>
                        <li>March 2017</li>
                        <li>June 2016</li>
                        <li>May 2016</li>
                        <li>January 2016</li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="footer-bottom">
    <div class="wrap">
        <div class="copy">
            <p> © 2017 All rights Reserved | Powered by <a href="#">Innovation Africa</a></p>
        </div>
    </div>
</div>
</body>
</html>



