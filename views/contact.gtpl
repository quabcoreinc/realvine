
<!DOCTYPE HTML>
<html>
<head>
    <title>Real Vine Juice  | Contact</title>
    <link href="../static/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Merriweather+Sans' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="header-box"></div>
<div class="wrap">
    <div class="total">
        <div class="header">
            <div class="header-bot">
                <div class="logo">
                    <a href="index.html"><img src="../static/images/logo.png" alt=""/></a>
                </div>
                <ul class="follow_icon">
                    <li><a href="#"><img src="../static/images/fb1.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/rss.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/tw.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/g+.png" alt=""></a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="search-bar">
                <input type="text" class="textbox" value=" Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
                <input name="searchsubmit" type="image" src="../static/images/search-icon.png" value="Go" id="searchsubmit" class="btn">
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="menu">
            <div class="top-nav">
                <ul>
                    <li><a href="/">Home</a></li> |
                    <li><a href="about">About</a></li> |
                    <li><a href="blog">Blog</a></li> |
                    <li><a href="services">Services</a></li> |
                    <li class="active"><a href="contact">Contact</a></li>
                </ul>
            </div>
        </div>
        <div class="banner">
        </div>
    </div>
    <div class="main">
        <div class="section group">
            <div class="col span_2_of_contact">
                <div class="contact-form">
                    <h3>Make an Order</h3>
                    <form id="the-contact-form" action="/contact">
                        <input type="hidden" value="{{.Token}}"/>
                        <span><label style="color:red;">{{.Error}}</label></span>
                        <span><label style="color:green;">{{.Success}}</label></span>
                        <div>
                            <span><label>Name</label></span>
                            <span><input name="userName" type="text" class="textbox" required=""/></span>
                        </div>
                        <div>
                            <span><label>E-Mail</label></span>
                            <span><input name="userEmail" type="text" class="textbox" required=""/></span>
                        </div>
                        <div>
                            <span><label>Mobile</label></span>
                            <span><input name="userPhone" type="text" class="textbox" required=""/></span>
                        </div>
                        <div>
                            <span><label>Flavour of Drink</label></span>
                            <span><input name="flavor" type="text" class="textbox" required=""/></span>
                        </div>
                        <div>
                            <span><label>No. of Bottles</label></span>
                            <span><input name="bottles" type="text" class="textbox" required=""/></span>
                        </div>
                        <div>
                            <span><label>Type of Occasion</label></span>
                            <span><input name="occasion" type="text" class="textbox" /></span>
                        </div>
                        <div>
                            <span><label>Mode of Payment</label></span>
                            <span><input name="payment" type="text" class="textbox" /></span>
                        </div>

                        <div>
                            <span><input type="submit" value="Submit" onclick="submitForm()"/></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col span_1_of_contact">
                <div class="company_address">
                    <h3>Find Us Here</h3>
                    <p>Main office, Gicel estate,</p>
                    <p>Block E 38/346, Weija, Accra</p>

                    <p>Phone:(+233) 244 474 762</p>
                    <p>Mobile: (+233) 262 474 762</p>
                    <p>Email: <span>truevinejuices@gmail.com</span></p>
                    <p>Contact: <span>Emmanuel Opare Addo</span>, <span>CEO</span></p>
                </div>
                <div class="company_address">
                    <h3>Contact</h3>
                    <p>Marlon Kuuku Crentsil</p>
                    <p>Business Development Officer</p>

                    <p>Phone:(+233) 244 494 555</p>
                    <p>Fax: (+233) 265 373 005</p>
                    <p>Email: <span>malcrent@gmail.com</span></p>
                    <p>Warehouse: <span>Dansoman Keep fit</span>, <span>Accra</span></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="footer-bottom">
    <div class="wrap">
        <div class="copy">
            <p> © 2017 All rights Reserved | Powered by <a href="#">Innovation Africa</a></p>
        </div>
    </div>
</div>



<script src="../static/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
    function submitForm(){
        $.ajax({
            method: "POST",
            data: $("#the-contact-form").serialize(),
            url:"/contact",
            success:function(data){
                alert("Sent successfully")
            },
            error:function (e, x, r) {
                alert("The process failed, please try again!")
            }
        });
        return false;
    }
    </script>

</body>
</html>



