
<!DOCTYPE HTML>
<html>
<head>
    <title>Real Vine Juice  | Admin </title>
    <style>
        .header{
            background-color: #EE412C;
            padding: 10px 10px 3px 3px;
            color:#ffffff;
        }
        tr:hover{
            background-color:lightgrey;
            cursor:pointer;
        }
        .menu-bar{
            background-color: gainsboro;
        }
        .menu-bar p {
            padding:5px 5px 5px 5px;
            -webkit-margin-before: 0em !important;
            -webkit-margin-after: 0em !important;
        }
        .menu-bar span:hover{
            background-color: #aaaaaa;
            padding:5px 5px;
            cursor:pointer;
        }
        thead tr th:hover{
            background-color: gainsboro;
        }
        #post-form{
            text-align: center;
        }
        input{
            height: 25px;
            padding-left: 2px;
            padding-right: 2px;
        }
        .btn{
            width: 100px;
            height: 30px;
        }
        .form-group{
            margin-top: 10px;
            margin-bottom:10px;
        }
    </style>
</head>
<body>
<div class="header">
    <div class="header-text">
        <h2>Real Vine Admin</h2>
    </div>
    <div style="text-align: right; margin-top:-30px;">
        <span >Logged in as kaypee90@yahoo.com <a href="/logout" style="color:#ffffff;">[ Logout ]</a></span>
    </div>
</div>
<div class="menu-bar">
    <p><span onclick="showListPage()">Posts</span> | <span onclick="showAddPage()">Add Post</span></p>
</div>

<div id="post-list">
    <h5>Home / Post / List</h5>
    <table width="100%">
        <thead>
        <tr style="text-align: left;">
            <th width="30%">Title</th>
            <th width="50%">Description</th>
            <th width="15%">Image Url</th>
            <th width="5%"></th>
        </tr>
        </thead>
        <tbody id="all-posts">
        {{range .Posts}}
        <tr id="post-row-{{.PK}}">
            <td> {{.Title}} </td>
            <td> {{.Description}} </td>
            <td> {{.ImageUrl}} </td>
            <td> <button class="delete-btn" onclick="deletePost({{.PK}})"> Delete </button> </td>
        </tr>
        {{end}}
        </tbody>
    </table>
</div>

<div id="post-add" style="display:none;">
    <h5>Home / Post / Add</h5>
    <form id="post-form" enctype="multipart/form-data">
        <input type="hidden" value="{{.Token}}"/>
        <div style="margin-bottom: 50px;">
            <h3> Add a new post</h3>
        </div>
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title"  required="" style="margin-left: 62px;width: 550px;" />
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" required="" style="margin-left: 20px;width: 550px;" rows="8" ></textarea>
        </div>
        <div class="form-group">
            <label></label>
            <input type="file" name="uploadfile"  style="margin-left: 0px;" />
        </div>

        <input type="button" name="addpost" value="Add Post" onclick="addPost()" class="btn" style="margin-left: 41px"/>
        <div  class="form-group">
            <label style="color:red;">{{.Error}}</label>
            <label style="color:green;">{{.Success}}</label>
        </div>
    </form>
</div>


<script src="../static/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">

    function showAddPage(){
        $("#post-add").show();
        $("#post-list").hide();
    }
    function showListPage(){
        $("#post-add").hide();
        $("#post-list").show();
    }


    function addPost(){
        var form = new FormData($("#post-form")[0]);
        $.ajax({
            method:"POST",
            url:"/admin",
            data:form,
            processData:false,
            contentType:false,
            success:function (data) {
                var post = JSON.parse(data);
                if(!post) return;
                appendPost(post);

                $('[name="title"]').val('');
                $('[name="description"]').val('');

                alert("Post added successfully!");
            },
            error:function (e, x, r) {
                alert("An error occured while adding post");
            }
        });

        return false;
    }

    function deletePost(pk) {
        var r = confirm("Are you sure you want to delete this post?");
        if (r == true) {
            $.ajax({
                method: "DELETE",
                url: "/admin?pk="+pk,
                success: function(data){
                    $("#post-row-" + pk).remove();
                    alert("Post deleted successfully!")
                }
            });
        } else {
            return
        }


    }

    function appendPost(post){
        $("#all-posts").append("<tr id='post-row-"+post.PK+"'><td>" + post.Title + "</td><td>" +
            post.Description + "</td><td>" + post.ImageUrl +"</td><td><button class='delete-btn' onclick='deletePost("+post.PK+
            ")'>Delete</button></td></tr>");
    }

</script>


</body>
</html>

