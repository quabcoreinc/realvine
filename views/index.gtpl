
<!DOCTYPE HTML>
<html>
<head>
    <title>Real Vine Juice  | Home </title>
    <link href="../static/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Merriweather+Sans' rel='stylesheet' type='text/css'>
    <!--slider-->
    <link rel="stylesheet" href="../static/css/flexslider.css" type="text/css" media="all" />
    <script src="../static/js/modernizr.js"></script>
    <!-- jQuery -->
    <script src="../static/js/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>
    <!-- FlexSlider -->
    <script defer src="../static/js/jquery.flexslider.js"></script>
    <script type="text/javascript">
        $(function(){
            SyntaxHighlighter.all();
        });
        $(window).load(function(){
            $('.flexslider').flexslider({
                animation: "slide",
                start: function(slider){
                    $('body').removeClass('loading');
                }
            });
        });
    </script>
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-70034014-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments)};
        gtag('js', new Date());

        gtag('config', 'UA-70034014-3');
    </script>

</head>
<body>
<div class="header-box"></div>
<div class="wrap">
    <div class="total">
        <div class="header">
            <div class="header-bot">
                <div class="logo">
                    <a href="index.html"><img src="../static/images/logo.png" alt=""/></a>
                </div>
                <ul class="follow_icon">
                    <li><a href="#"><img src="../static/images/fb1.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/rss.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/tw.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/g+.png" alt=""></a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="search-bar">
                <input type="text" class="textbox" value=" Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
                <input name="searchsubmit" type="image" src="../static/images/search-icon.png" value="Go" id="searchsubmit" class="btn">
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="menu">
            <div class="top-nav">
                <ul>
                    <li class="active"><a href="/">Home</a></li> |
                    <li><a href="about">About</a></li> |
                    <li><a href="blog">Blog</a></li> |
                    <li><a href="services">Services</a></li> |
                    <li><a href="contact">Contact</a></li>
                </ul>
            </div>
        </div>
        <div class="banner">
            <div class="flexslider">
                <ul class="slides">
                    <li><img src="../static/images/banner2.jpg" alt=""/></li>
                    <li><img src="../static/images/banner1.jpg"  alt=""/></li>
                    <li><img src="../static/images/banner3.jpg"  alt=""/></li>
                    <li><img src="../static/images/banner4.jpg"  alt=""/></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="section group">
            <div class="col_1_of_4 span_1_of_4">
                <img src="../static/images/pic.jpg" alt=""/>
                <div class="desc">
                    <div class="left-text">
                        <h5>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col_1_of_4 span_1_of_4">
                <img src="../static/images/pic1.jpg" alt=""/>
                <div class="desc">
                    <div class="left-text">
                        <h5>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col_1_of_4 span_1_of_4">
                <img src="../static/images/pic2.jpg" alt=""/>
                <div class="desc">
                    <div class="left-text">
                        <h5>
                        </h5>
                    </div>

                </div>
            </div>
            <div class="col_1_of_4 span_1_of_4 ">
                <img src="../static/images/pic3.jpg" alt=""/>
                <div class="last-desc">
                    <div class="left-text">
                        <h5>
                        </h5>
                    </div>

                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="content-middle">
            <div class="lsidebar span_1_of_3">
                <img src="../static/images/pic4.jpg" alt=""/>
            </div>
            <div class="cont span_2_of_3">
                <div class="test-meta"><img src="../static/images/quotes.png" alt=""/><span class="user">Emmanuel, </span><span class="info">Founder</span></div>
                <p class="paragraph"><a href="#">The juice is completely natural and our most popular product is the pineapple and ginger mix. We have other upcoming interesting cominations with hibiscus, moringa, tiger nuts, dandelion, noni, lemon and so much more... </a></p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="bottom-grids">
            <div class="bottom-grid1">
                <h3>POPULAR INFO</h3>
                <span>Our Distributors / Clients</span>
                <p>We have our main distributors in Accra with our clients all over the country. This include:</p>
                <ul>
                    <li><a href="#"><img src="../static/images/marker1.png"> Est western premier hotel</a></li>
                    <li><a href="#"><img src="../static/images/marker1.png"> Bake shop classics</a></li>
                    <li><a href="#"><img src="../static/images/marker1.png"> Le Bijou</a></li>
                    <li><a href="#"><img src="../static/images/marker1.png"> Marwako restaurant</a></li>
                    <li><a href="#"><img src="../static/images/marker1.png"> Dotkad supermarket</a></li>
                    <li><a href="#"><img src="../static/images/marker1.png"> Kinko ashgrove</a></li>
                    <li><a href="#"><img src="../static/images/marker1.png"> dolore magna aliqua</a></li>
                    <li><a href="#"><img src="../static/images/marker1.png"> Green grass grill</a></li>
                    <li><a href="#"><img src="../static/images/marker1.png"> Adepa aduane</a></li>
                    <li><a href="#"><img src="../static/images/marker1.png"> Fuel station marts across the capital</a></li>
                    <div class="clear"> </div>
                </ul><br>
                <a href='about' class='button'>Read More</a>
            </div>
            <div class="bottom-grid2 bottom-mid">
                <h3>Our Juice</h3>
                <span>Real Vine Natural Juice</span>
                <p>Real vine is a freshly squeezed 100% premium fruit, herbs, flowers, leaves or any product of a plant as required. There is no added sugar, no preservations, no color and no flavors.</p>
                <div class="gallery">
                    <ul>
                        <li><a href="../static/images/t-pic5.jpg"><img src="../static/images/pic5.jpg" alt=""></a></li>
                        <li><a href="../static/images/t-pic6.jpg"><img src="../static/images/pic6.jpg" alt=""></a></li>
                        <li><a href="../static/images/t-pic7.jpg"><img src="../static/images/pic7.jpg" alt=""></a></li>
                        <li><a href="../static/images/t-pic8.jpg"><img src="../static/images/pic8.jpg" alt=""></a></li>
                        <li><a href="../static/images/t-pic9.jpg"><img src="../static/images/pic9.jpg" alt=""></a></li>
                        <li><a href="../static/images/t-pic10.jpg"><img src="../static/images/pic10.jpg" alt=""></a></li>
                        <li><a href="../static/images/t-pic11.jpg"><img src="../static/images/pic11.jpg" alt=""></a></li>
                        <li><a href="../static/images/t-pic12.jpg"><img src="../static/images/pic12.jpg" alt=""></a></li>

                        <div class="clear"> </div>
                    </ul><br>
                    <a href='about' class='button'>Read More</a>
                </div>
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

                <script type="text/javascript" src="../static/js/jquery.lightbox.js"></script>
                <link rel="stylesheet" type="text/css" href="../static/css/lightbox.css" media="screen">
                <script type="text/javascript">
                    $(function() {
                        $('.gallery a').lightBox();
                    });
                </script>
            </div>
            <div class="bottom-grid1 bottom-last">
                <h3>Latest INFO</h3>
                <span>Get Facts & Info About Real Vine</span>
                <p>Real Vine juice brand is produced by a Ghanaian registered juice processing company</p>
                <p>Real Vine juice brand has accreditation from the Food and Drugs Authority.</p>
                <p>Real vine had it humble beginning in the kitchen of Emmanuel Opare Addo the founder. It was there that he created the first juice that soon became the basis for Real Vine.</p>
                <p>Real Vine can now be customized with labels i.e.  either text or pictures or both for for your special events.</p>
                <p>Real Vine has its main factory at Weija Gicel Estate, Ghana.</p>
                <p>Real Vine is available in Fuel station marts across the capital.</p>
                <br>
                <a href='about' class='button'>Read More</a>
            </div>
            <div class="clear"> </div>
        </div>
        <div class="clear"> </div>
    </div>
</div>
<div class="footer-bottom">
    <div class="wrap">
        <div class="copy">
            <p> © 2017 All rights Reserved | Powered by <a href="#">Innovation Africa</a></p>
        </div>
    </div>
</div>
</body>
</html>



