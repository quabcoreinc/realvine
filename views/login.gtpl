
<!DOCTYPE HTML>
<html>
<head>
    <title>Real Vine Juice  | Login </title>
    <style>
        input{
            height: 25px;
            padding-left: 2px;
            padding-right: 2px;
        }
        .btn{
            width: 100px;
            height: 30px;
        }
        #login-form{
            text-align: center;
            margin-top:9%;
        }
        .form-group{
            margin-top: 10px;
            margin-bottom:10px;
        }
    </style>

</head>
<body>

<form id="login-form" action="/login" method="post">
    <div style="margin-bottom: 50px;">
       <h2> Login</h2>
    </div>
    <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" placeholder="Username" required="" style="margin-left: 20px"/>
    </div>
    <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" placeholder="Password" required="" style="margin-left: 24px"/>
    </div>
    <input type="submit" name="submit" value="Login" class="btn" style="margin-left: 23px"/>
    <div  class="form-group">
        <label style="color:red;">{{.Error}}</label>
    </div>
</form>
</body>
</html>