
<!DOCTYPE HTML>
<html>
<head>
    <title>Real Vine Juice  | About </title>
    <link href="../static/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Merriweather+Sans' rel='stylesheet' type='text/css'>
    <style>
        .video-container {
            position: relative;
            padding-bottom: 56.25%;
            padding-top: 30px; height: 0; overflow: hidden;
        }

        .video-container iframe,
        .video-container object,
        .video-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<body>
<div class="header-box"></div>
<div class="wrap">
    <div class="total">
        <div class="header">
            <div class="header-bot">
                <div class="logo">
                    <a href="index.html"><img src="../static/images/logo.png" alt=""/></a>
                </div>
                <ul class="follow_icon">
                    <li><a href="#"><img src="../static/images/fb1.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/rss.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/tw.png" alt=""></a></li>
                    <li><a href="#"><img src="../static/images/g+.png" alt=""></a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="search-bar">
                <input type="text" class="textbox" value=" Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
                <input name="searchsubmit" type="image" src="../static/images/search-icon.png" value="Go" id="searchsubmit" class="btn">
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="menu">
            <div class="top-nav">
                <ul>
                    <li><a href="/">Home</a></li> |
                    <li class="active"><a href="about">About</a></li> |
                    <li><a href="blog">Blog</a></li> |
                    <li><a href="services">Services</a></li> |
                    <li><a href="contact">Contact</a></li>
                </ul>
            </div>
        </div>
        <div class="banner">
        </div>
    </div>
    <div class="main">
        <div class="section group">
            <div class="lsidebar1 span_1_of_a offers_list">
                <h3>Testimonials</h3>
                <div class="testimonials">
                    <h3>Marian</h3>
                    <p><span class="quotes"></span>It's a good product and anyone who tastes it enjoys it.<span class="quotes-down"></span></p>
                </div>
                <div class="testimonials">
                    <h3>Raymond</h3>
                    <p><span class="quotes"></span>Its a product served on all occasions and it taste add up to its success.<span class="quotes-down"></span></p>
                </div>
                <div class="testimonials">
                    <h3>Kwabena</h3>
                    <p><span class="quotes"></span>I tasted it and saw that it was good! A healthy delicious juice...<span class="quotes-down"></span></p>
                </div>
            </div>
            <div class="cont1 span_2_of_a about_desc">
                <h2>About Us</h2>
                <p>TVP is a Ghanaian registered juice processing company which produces the Real Vine juice brand.</p>
                <p>Real vine is a ffreshly squeezed 100% premium fruit, hers, flowers, leaves or any product of a plant as required. There is no added suar no preservations, no color and no flavors.
                    Real vine had it humble beginning in the kitchen of Emmanuel Opare Addo the founder. It was there that he created the first juice that has become the basis for Real Vine. Initially he experimented with his blender and gave out samples to his friends family and the general public.
                </p>
                <div class="" style="margin-top: 20px">
                    <div class="">
                        <div class="video-container">
                            <iframe width="804" height="380" src="https://www.youtube.com/embed/KBeiNtdvIvI" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>

                </div>
            </div>
            <div class="clear"></div>
        </div>

    </div>
</div>
<div class="footer-bottom">
    <div class="wrap">
        <div class="copy">
            <p> © 2017 All rights Reserved | Powered by <a href="#">Innovation Africa</a></p>
        </div>
    </div>
</div>
</body>
</html>



