package util

import(
  "github.com/robfig/cron"
	"strings"
	"log"
)


type ICustService interface {
	startService(f func())
	addTaskToService(f func())
	stopService()
}

type CustService struct {
        IsRunning bool
	TimeInterval string
}

var c = cron.New()

func (t CustService)startService(f func()){
	c.AddFunc(t.TimeInterval, f)
	if t.IsRunning == false {
		log.Println("Starting Service")
		c.Start()
	}
	t.IsRunning = true
	log.Println("Service started")

}

func (t CustService)addTaskToService(f func()){
	c.AddFunc(t.TimeInterval, f)
	log.Println("Adding task to Service" )
}

func (t CustService)stopService(){
	if t.IsRunning == true{
		log.Println("Stopping Service at")
		c.Stop()
	}
	t.IsRunning = false;
	log.Println("Service stopped")
}


func ServiceUtility(i ICustService, action string, f func())  {
	if strings.ToUpper(action) == "START"{
		i.startService(f)
	}else if strings.ToUpper(action) == "STOP"{
		i.stopService()
	}else if strings.ToUpper(action) == "ADD"{
		i.addTaskToService(f)
	}else {
		panic("Unknown action passed to service utility")
	}
}