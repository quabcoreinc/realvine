package models

type User struct {
	Username string `db:"username"`
	Password []byte `db:"password"`
}

type Post struct {
	PK int64 `db:"pk"`
	ImageUrl string `db:"imageurl"`
	Title string `db:"title"`
	Description string `db:"description"`
	PostedOn string `db:"postedon"`
	User string `db:"user"`
}

type Email struct {
	PK int `db:"pk"`
        From string `db:"from"`
	To string `db:"to"`
	Subject string `db:"subject"`
	Phone string `db:"phone"`
	Body string `db:"body"`
	SendersName string `db:"sendersname"`
	CreatedOn string `db:"createdon"`
	IsSent bool `db:"issent"`
}

