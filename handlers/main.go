package handlers

import (
	"net/http"
	"time"
	"crypto/md5"
	"io"
	"strconv"
	"html/template"
	"github.com/RealVineWeb/helpers"
	"github.com/goincremental/negroni-sessions"
	"golang.org/x/crypto/bcrypt"
	"github.com/RealVineWeb/models"
	"encoding/json"
	"strings"
	"os"
	"log"
	"github.com/RealVineWeb/util"
	"fmt"
	"github.com/RealVineWeb/emailing"
)




//Initalize class for db activities
var t = helpers.T{}
//Initialize and configure custservice object to run cron job
var s = util.CustService{TimeInterval:"2 * * * * *"}

//Initialize database connection
func initDb(i helpers.I){
	i.InitializeDb()
}


func sendEmail(){
	log.Println("Initializing mail sending")
	emails := []models.Email{}
	if err := t.GetEmails(&emails); err!=nil{
		log.Println(err.Error())
	}else {
		log.Println(strconv.Itoa(len(emails)) + " email(s) fetched");
                for _, email := range emails{
                    if err := emailing.SendEmail(strings.Split(email.To, ";"),
                         email.SendersName, email.Phone, email.From, email.Subject, email.Body); err != nil{
	                 //mail sending failed
	                 log.Println("Email sending failed")
                    }else {
		        email.IsSent = true
	               if err := t.UpdateEmail(&email); err != nil{
		           log.Println("Updating DB after sending email failed")
	            }
        }
     }
}


}

func (a *App)HandleService(){
	//Initialize db
	initDb(&t)
	//Commented out to stop service from running on production
	/*
	//stop running services before you start a new one when application starts
	if s.IsRunning{
		util.ServiceUtility(s, "stop", nil)
	}
	//Check if emails service should be activated
	if helpers.ActivateEmailService() == true{
		util.ServiceUtility(s, "start", sendEmail)
	}
       */
}

func (a *App)LoginHandler(w http.ResponseWriter, r *http.Request){
	var p helpers.Page;
	if r.Method == "POST"{
		var username = template.HTMLEscapeString(r.FormValue("username"))
		var password = template.HTMLEscapeString(r.FormValue("password"))
		user, err := t.GetUserByUsername(username)
		if err != nil{
			p.Error = err.Error()
		}else if user == nil{
			p.Error="Invalid Username Or Password"
		}else{

			if err = bcrypt.CompareHashAndPassword(user.Password, []byte(password)); err !=nil{
				p.Error="Invalid Username Or Password"
			}else{
				sessions.GetSession(r).Set("User", user.Username)
				http.Redirect(w, r, "/admin", http.StatusFound)
				return
			}

		}
	}
		w.Header().Set("Content-Type", "text/html")
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))
	        p.Token = token
		if t, err := template.ParseFiles("views/login.gtpl"); err !=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}else {
			t.Execute(w, p)
		}



}

func (a *App)AdminHandler(w http.ResponseWriter, r *http.Request){
	var p helpers.Page;
	if r.Method == "GET"{
		w.Header().Set("Content-Type", "text/html")
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))
		p = helpers.Page{Posts: []models.Post{}, Token:token, User:a.GetStringFromSession(r, "User")}
		if err := t.GetPosts(&p.Posts); err != nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if t, err := template.ParseFiles("views/admin.gtpl"); err !=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}else {
			t.Execute(w, p)
		}


	}else if r.Method == "DELETE"{
		var p models.Post;
		pk, err := strconv.ParseInt(r.FormValue("pk"), 10, 16)
		if(err != nil){
			log.Println(err.Error())
		}
		if err := t.GetPostById(&p, pk); err!=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(err.Error())
			return
		}

		if err := t.DeletePost(&p); err!=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(err.Error())
			return
		}
		//send a status successful to ajax request
		w.WriteHeader(http.StatusOK)
        }else if r.Method == "POST"{
		var imageUrl string;

		r.ParseMultipartForm(32 << 20)
		file, handler, err := r.FormFile("uploadfile")
		if err != nil{
			file = nil
			handler = nil
			imageUrl = "../static/images/b-img.jpg";
			log.Println(err.Error())
		}else {
			defer file.Close()

			if handler != nil{
			    f, err := os.OpenFile("./static/uploads/"+handler.Filename, os.O_WRONLY | os.O_CREATE, 0666)
			    if err != nil{
			    log.Println("Error whiles handling upload file")
			    http.Error(w, err.Error(), http.StatusInternalServerError)
			     return
			   }
			   defer f.Close()
			     io.Copy(f, file)

			   imageUrl = "../static/uploads/"+handler.Filename
			}

		}

		p:= models.Post{
			PK:-1,
			Title:strings.ToUpper(template.HTMLEscapeString(r.FormValue("title"))),
			Description:template.HTMLEscapeString(r.FormValue("description")),
			ImageUrl:imageUrl,
			User:a.GetStringFromSession(r, "User"),
			PostedOn:strconv.FormatInt(time.Now().Unix(), 10),
		}
		if err := t.AddPost(&p); err !=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(err.Error())
			return
		}
		//write json response to http response
		if err := json.NewEncoder(w).Encode(p);err!=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(err.Error())
		}
	}else {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
}

func (a *App)ErrorHandler(w http.ResponseWriter, r *http.Request){
	if r.Method == "GET"{
		w.Header().Set("Content-Type", "text/html")
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))
		if t, err := template.ParseFiles("views/404.gtpl"); err !=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}else {
			t.Execute(w, token)
		}


	}
}

func (a *App)IndexHandler(w http.ResponseWriter, r *http.Request){
	if r.Method == "GET"{
		w.Header().Set("Content-Type", "text/html")
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))
		if t, err := template.ParseFiles("views/index.gtpl"); err !=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}else {
			t.Execute(w, token)
		}
	}
}

func (a *App)AboutHandler(w http.ResponseWriter, r *http.Request){
	if r.Method == "GET"{
		w.Header().Set("Content-Type", "text/html")
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))
		if t, err := template.ParseFiles("views/about.gtpl"); err !=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}else {
			t.Execute(w, token)
		}
	}
}

func (a *App)BlogHandler(w http.ResponseWriter, r *http.Request){
	var p helpers.Page;
	if r.Method == "GET"{
		w.Header().Set("Content-Type", "text/html")
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))
		p = helpers.Page{Posts: []models.Post{}, Token:token, User:a.GetStringFromSession(r, "User")}
		if err := t.GetPosts(&p.Posts); err!=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if t, err := template.ParseFiles("views/menu.gtpl"); err !=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}else {
			t.Execute(w, p)
		}
	}
}

func (a *App)ServicesHandler(w http.ResponseWriter, r *http.Request){
	if r.Method == "GET"{
		w.Header().Set("Content-Type", "text/html")
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))
		if t, err := template.ParseFiles("views/services.gtpl"); err !=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}else {
			t.Execute(w, token)
		}
	}
}

func (a *App)ContactHandler(w http.ResponseWriter, r *http.Request){
	var p helpers.Page;
	if r.Method == "GET"{
		w.Header().Set("Content-Type", "text/html")
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))
		p.Token = token
		if t, err := template.ParseFiles("views/contact.gtpl"); err !=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}else {
			t.Execute(w, p)
		}
	}else if r.Method == "POST"{
		userName := template.HTMLEscapeString(r.FormValue("userName"))
		userEmail := template.HTMLEscapeString(r.FormValue("userEmail"))
		phone := template.HTMLEscapeString(r.FormValue("userPhone"))
		flavour := template.HTMLEscapeString(r.FormValue("flavor"))
		bottles := template.HTMLEscapeString(r.FormValue("bottles"))
		payment := template.HTMLEscapeString(r.FormValue("payment"))
		occasion := template.HTMLEscapeString(r.FormValue("occasion"))

		message := "Flavour of Drink: " + flavour +"\n" +
				"No of Bottles: " + bottles + "\n" +
					"Mode of Payment: " + payment + "\n" +
						"Type of Occasion: " + occasion + "\n"

		if(userName =="" || userEmail == "" || phone == ""){
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		email := models.Email{
			PK:-1,
			From:userEmail,
			To:"asantekwabena2013@gmail.com;kaypee90@yahoo.com",
			Subject:"Sent from realvinegh.com",
			Phone: phone,
			Body: message,
			SendersName:userName,
			CreatedOn:time.Now().String(),
		}

		if err:=t.AddEmail(&email);err!=nil{
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(err.Error())
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}

func (a *App)LogoutHandler(w http.ResponseWriter, r *http.Request){
	if r.Method == "GET"{
		sessions.GetSession(r).Set("User", nil)
		http.Redirect(w, r, "/admin", http.StatusTemporaryRedirect)
		return
	}
}


func (a *App)VerifyDatabase(w http.ResponseWriter, r *http.Request, next http.HandlerFunc){
	//initDb(t)
	if result := t.PingDb(); result == false{
		http.Error(w, "Database unavailable!", http.StatusInternalServerError)
		return
	}

	next(w, r)
}

func (a *App)VerifyUser(w http.ResponseWriter, r *http.Request, next http.HandlerFunc){
	if r.URL.Path == "/admin" ||  r.URL.Path == "/login" {
		if username := a.GetStringFromSession(r, "User"); username != "" {
			if user, _ := t.GetUserByUsername(username); user != nil {
				if r.URL.Path == "/admin" {
				    next(w, r)
				    return
				}else{
					http.Redirect(w, r, "/admin", http.StatusTemporaryRedirect)
					return
				}
			}
		}
		if r.URL.Path == "/admin" {
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
			return
		}

	}
	next(w, r)

}

func (a *App)GetStringFromSession(r *http.Request, key string) string{
	var strVal string
	sessionVal := sessions.GetSession(r)
	if sessionVal != nil{
		if val := sessionVal.Get(key);val!=nil{
			strVal = val.(string)
		}
	}	
	return strVal;
}