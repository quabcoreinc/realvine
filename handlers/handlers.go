package handlers

import (
	gmux "github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"github.com/goincremental/negroni-sessions/cookiestore"
	"github.com/goincremental/negroni-sessions"
	"net/http"
	"fmt"
 )
 
 type App struct{
 
		 Router *gmux.Router
 }

 
 func (a *App) Initialize(){
	 go a.HandleService()
 
	 a.Router = gmux.NewRouter()
	 a.InitializeRoutes()
 }
 
 func (a *App) InitializeRoutes(){	
 
	 a.Router.HandleFunc("/", a.IndexHandler).Methods("GET")
	 a.Router.HandleFunc("/about", a.AboutHandler).Methods("GET")
	 a.Router.HandleFunc("/blog", a.BlogHandler).Methods("GET")
	 a.Router.HandleFunc("/services", a.ServicesHandler).Methods("GET")
	 a.Router.HandleFunc("/contact", a.ContactHandler)
	 a.Router.HandleFunc("/error", a.ErrorHandler).Methods("GET")
	 a.Router.HandleFunc("/login", a.LoginHandler)
	 a.Router.HandleFunc("/admin", a.AdminHandler)
	 a.Router.HandleFunc("/logout", a.LogoutHandler)
 }
 
 func (a *App) Run(addr string){	
	 fmt.Println("listening...")
 
	 n:= negroni.Classic()
	 store := cookiestore.New([]byte("SECRET_KEY_123"))
	 store.Options(sessions.Options{MaxAge:1800})
	 n.Use(sessions.Sessions("my_session", store))
	 n.Use(negroni.HandlerFunc(a.VerifyDatabase))
	 n.Use(negroni.HandlerFunc(a.VerifyUser))
	 n.Use(negroni.NewStatic(http.Dir(".")))
	 n.UseHandler(a.Router)
	 n.Run(addr)
 }